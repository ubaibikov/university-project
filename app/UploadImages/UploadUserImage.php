<?php

namespace App\UploadImages;

use Illuminate\Http\Request;

class UploadUserImage
{
    protected $imagePath = 'images/users';
    private $request;
    private $imageName;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function createUploadedUserImage(int $userId, string $fileUpload)
    {
        return $this->createUserProfileImage($this->createImageUserProfileName($userId, $fileUpload), $fileUpload);
    }

    protected function createImageUserProfileName(int $userId, string $fileUpload): string
    {

        $this->imageName = time() . '_user_profile_' . $userId . '.' . $this->request->$fileUpload->extension();

        return $this->imageName;
    }

    protected function createUserProfileImage(string $imageProfileName, string $fileUpload)
    {
        return $this->request->$fileUpload->move(public_path($this->imagePath), $imageProfileName);
    }

    public function getRelativeUserProfileImagePath(): string
    {
        return $this->imagePath . '/' . $this->imageName;
    }
}
