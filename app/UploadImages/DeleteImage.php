<?php

namespace App\UploadImages;

class DeleteImage
{
    public static function deleteImageWherePublicPath(string $publicPath)
    {
        return unlink($publicPath);
    }
}
