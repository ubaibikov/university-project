<?php

namespace App\UploadImages;

use Illuminate\Http\Request;
use App\UploadImages\UploadUserImage;
use Illuminate\Support\Facades\Auth;
use App\UserProfileImage;

class UploadImageFacade
{
    private $authId;
    private $uploadUserImage;

    public function __construct(Request $request, $uploadFileData, $authId)
    {
        $this->uploadUserImage = new UploadUserImage($request);
        $this->authId = $authId;

        if (!empty($request->$uploadFileData)) {
            if ($this->createImage($uploadFileData)) {
                $this->createUserProfileImageData();
            }
        } else {
            return;
        }
    }

    private function createImage(string $uploadFileData)
    {
        return $this->uploadUserImage->createUploadedUserImage($this->authId, $uploadFileData);
    }

    private function createUserProfileImageData()
    {
        UserProfileImage::create([
            'user_id' => $this->authId,
            'image_relative_path' => $this->uploadUserImage->getRelativeUserProfileImagePath()
        ]);
    }

    public static function getAssetImageUrl(Request $request, int $userId)
    {
        return self::getAssetImageUrlWhereUserId($request, $userId);
    }

    public static function getAssetImageUrlWhereUserId(Request $request, int $userId)
    {
        $imageUrl = UserProfileImage::select('image_relative_path')->where('user_id', $userId)->get()->first();
        $imageUrl = json_decode($imageUrl, true);
        if ($imageUrl['image_relative_path']) {
            return asset($imageUrl['image_relative_path']);
        }
        return null;
    }

    public static function getUserCurrentPublicImagePath(string $id)
    {
        if (self::isExistUserProfileImageWhereUserId($id)) {
            return public_path(str_replace('/', '\\', static::getUserFindImageRelativePath($id))); //FIXED ME
        }

        return null;
    }

    protected static function getUserFindImageRelativePath(string $id)
    {
        return UserProfileImage::where('user_id', $id)->first()->image_relative_path;
    }

    public static function isExistUserProfileImageWhereUserId($id)
    {
        return UserProfileImage::where('user_id', '=', $id)->exists();
    }
}
