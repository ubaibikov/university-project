<?php

namespace App\Admin;


trait AdminTrait
{
    public static $currentAdminPath = 'admin/admin_panel/';

    public $adminPanelPath = '/admin_panel';

    public function adminPanelRedirect(string $currentRedirect)
    {
        return redirect($this->adminPanelPath . $currentRedirect);
    }
}
