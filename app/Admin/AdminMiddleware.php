<?php

namespace App\Admin;

use \Illuminate\Http\Request;

class AdminMiddleware
{
    public static function verifiedAdminLogin($adminLogin): bool
    {
        return self::isVerifiedAdminAdminData($adminLogin, 'LOGIN') ?? false;
    }

    public static function verifiedAdminPassword($adminPassword): bool
    {
        return self::isVerifiedAdminAdminData($adminPassword, 'PASSWORD') ?? false;
    }

    private static function isVerifiedAdminAdminData($verifiedData, $envDataKey): bool
    {
        if (!empty($verifiedData)) {
            if (self::envAdmin($envDataKey) == trim($verifiedData)) {
                return true;
            }
        }
        return false;
    }

    public static function setAdminSessionCheckedData(Request $request, bool $value)
    {
        $request->session()->put('admin.adminChecked', $value);
    }

    public static function getAdminSessionCheckedData(Request $request)
    {
        return $request->session()->get('admin.adminChecked');
    }


    public static function envAdminRedirect()
    {
        return redirect(self::envAdmin('ROUTE'));
    }

    public static function adminPanelRedirect()
    {
        return redirect('/admin_panel');
    }

    private static function envAdmin(string $key)
    {
        return env('ADMIN_' . $key);
    }
}
