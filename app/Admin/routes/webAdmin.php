<?php

use Illuminate\Support\Facades\Route;

Route::get('/admin_panel','AdminPanelController@index');
Route::post('/admin_panel','AdminPanelController@adminLogout')->name('adminLogout');

Route::resource('/admin_panel/universities', 'UniversityController')->name('UniversityController@store','universities');

Route::resource('/admin_panel/countries', 'CountryController');

Route::resource('/admin_panel/teachers', 'TeacherController');

Route::get('/admin_panel/users','AdminPanelUsersController@index')->name('adminPanelUsers');
Route::delete('/admin_panel/users/delete/{id}','AdminPanelUsersController@destroy')->name('adminPanelUsersDelete');
