<?php

namespace App\Admin;

use Illuminate\Support\Facades\Route;

class AdminPanel
{
    protected static $middleware = 'admin.verified';

    public static function routes()
    {
        return Route::group(['middleware' => self::$middleware], function(){
            return self::adminPanelRoutes();
        });
    }

    protected static function adminPanelRoutes()
    {
        require app_path_str('Admin/routes/webAdmin.php');
    }
}
