<?php

namespace App\DataBaseTraits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;

trait QueriesTrait
{
    public function getModelAllData(Model $model)
    {
        return $model::all();
    }

    public function getModelFindId($model, string $id)
    {
        return $model::find($id);
    }

    public function getModelTable(string $table): Builder
    {
        return DB::table($table);
    }
}
