<?php

namespace App\DataBaseTraits;

use Illuminate\Database\Eloquent\Model;
use App\UploadImages\UploadImageFacade;
use App\UserProfileImage;
use App\UploadImages\DeleteImage;

trait UserQueriesTrait
{
    public function deleteUser($userModel)
    {
        $this->deleteUserProfileImage($userModel->id);
        return $userModel->delete();
    }

    protected function deleteUserProfileImage(string $id)
    {
        if ($this->isExistsDeletedPublicPathImage($id)) {
            if ($this->deleteUserProfileImageWherePublicPath($this->getCurrentUserProfileImagePath($id))) {
                return $this->deleteUserProfileImageTableRow($id);
            }
        }
        return null;
    }

    private function deleteUserProfileImageWherePublicPath(string $publicPath)
    {
        return DeleteImage::deleteImageWherePublicPath($publicPath);
    }

    public function getCurrentUserProfileImagePath(string $id)
    {
        return UploadImageFacade::getUserCurrentPublicImagePath($id);
    }

    protected function deleteUserProfileImageTableRow(string $id)
    {
        return UserProfileImage::where('user_id', $id)->delete();
    }

    protected function isExistsDeletedPublicPathImage(string $id)
    {
        return UploadImageFacade::isExistUserProfileImageWhereUserId($id);
    }
}
