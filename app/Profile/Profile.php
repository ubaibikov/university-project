<?php

namespace App\Profile;

use Illuminate\Support\Facades\DB;
use App\UploadImages\UploadImageFacade;
use Illuminate\Http\Request;

class Profile
{
    public static function getUserProfileData(string $id)
    {
        $user = DB::table('users')
            ->join('universities', 'users.university_id', '=', 'universities.id')
            ->join('countries', 'users.country_id', '=', 'countries.id')
            ->select('users.*', 'universities.university_name', 'countries.country_name')
            ->where('users.id', $id)
            ->get()
            ->first();
        return $user;
    }

    public static function getUserProfileImageData(Request $request, string $id)
    {
        return UploadImageFacade::getAssetImageUrlWhereUserId($request, $id);
    }
}
