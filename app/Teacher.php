<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable = [
        'name', 'last_name', 'patronymic',
        'email', 'password', 'is_changed_password',
        'university_id','country_id',
    ];

    protected $hidden = [
        'password',
    ];

}
