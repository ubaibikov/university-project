<?php

namespace App\Univer\Controllers;

use App\Country as AppCountry;
use App\Univer\Communication;

class Country implements Communication
{
    private static $mainRedirect = '/admin_panel/countries';

    public static function getValidate(\Illuminate\Http\Request $request)
    {
        return $request->validate([
            'countryName' => 'required',
        ]);
    }

    public static function modelFind(string $id)
    {
        return AppCountry::find($id);
    }

    public static function mainRedirect()
    {
        return redirect(self::$mainRedirect);
    }
}
