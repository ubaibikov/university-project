<?php

namespace App\Univer;
use Illuminate\Http\Request;


interface Communication
{
    public static function getValidate(Request $request);
    public static function modelFind(string $id);
    public static function mainRedirect();
}
