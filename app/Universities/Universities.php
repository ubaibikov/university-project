<?php

namespace App\Universities;
use App\University;
use Illuminate\Http\Request;

class Universities
{
    public static function universityMainRedirect()
    {
        return redirect('/admin_panel/universities');
    }

    public static function unifersityFind(string $id)
    {
        return University::find($id);
    }

    public static function getUnivercityValiation(Request $request)
    {
        return $request->validate([
            'universityName' => 'required',
        ]);
    }

}
