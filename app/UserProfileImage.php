<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfileImage extends Model
{
        /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'image_relative_path',
    ];
}
