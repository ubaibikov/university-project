<?php

namespace App\Http\Middleware;

use App\Admin\AdminMiddleware;
use \Illuminate\Http\Request;
use Closure;

use function Psy\debug;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $adminMiddleware = AdminMiddleware::class;

        if ($adminMiddleware::verifiedAdminLogin($request->adminEmail) && $adminMiddleware::verifiedAdminPassword($request->adminPassword)) {
            $adminMiddleware::setAdminSessionCheckedData($request, true);
            return $adminMiddleware::adminPanelRedirect();
        }

        if(!$adminMiddleware::getAdminSessionCheckedData($request)){
            return $adminMiddleware::envAdminRedirect();
        }

        return $next($request);
    }
}
