<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ProfileCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // rewrate to providers

        if (Auth::id() == $request->id) {
            return $next($request);
        }

        return redirect(route('profile-view', $request->id));
    }
}
