<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile\Profile;

class ProfileEditController extends Controller
{

    public function profile($id)
    {
        $user = Profile::getUserProfileData($id);
        $userAvatarImageUrl = Profile::getUserProfileImageData(request(),$id);

        return view('profile/update',compact('user','userAvatarImageUrl'));
    }

    public function create(Request $request)
    {

    }
}
