<?php

namespace App\Http\Controllers;
use App\Univer\Controllers\Country as UniverCountry;
use App\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public static $currentUniversityPath = 'admin/admin_panel/';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        return view(self::$currentUniversityPath . 'countries', compact('countries'));
    }

    public function store(Request $request)
    {
        UniverCountry::getValidate($request);

        $countryMake = new Country([
            'country_name' => $request->get('countryName'),
        ]);

        $countryMake->save();

        return UniverCountry::mainRedirect()->with('success', 'Contact saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = UniverCountry::modelFind($id);
        return view(self::$currentUniversityPath . 'countries_update', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editdata = [
            'country_name' => $request->countryName,
        ];
        Country::where('id', $id)->update($editdata);
        return UniverCountry::mainRedirect()->with('success', 'Contact saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = UniverCountry::modelFind($id);
        $country->delete();

        return UniverCountry::mainRedirect()->with('success', 'Contact saved!');
    }
}
