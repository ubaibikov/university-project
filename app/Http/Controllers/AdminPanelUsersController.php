<?php

namespace App\Http\Controllers;

use App\User;
use App\UserProfileImage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\UploadImages\UploadImageFacade;

class AdminPanelUsersController extends Controller
{
    use \App\Admin\AdminTrait;
    use \App\DataBaseTraits\QueriesTrait;
    use \App\DataBaseTraits\UserQueriesTrait;
    public function index()
    {
        $users = $this->getModelTable('users')
        ->join('universities', 'users.university_id', '=', 'universities.id')
        ->join('countries', 'users.country_id', '=', 'countries.id')
        ->select('users.*','universities.university_name','countries.country_name')
        ->paginate(15);

        return view(self::$currentAdminPath.'users',compact('users'));
    }

    public function destroy($id)
    {
        $user = $this->deleteUser($this->getModelFindId(User::class,$id));
        return $this->adminPanelRedirect('/users');
    }
}
