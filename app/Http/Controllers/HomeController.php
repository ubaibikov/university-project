<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\University;
use App\UploadImages\UploadImageFacade;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $universityName = University::select('university_name')->where('id',Auth::user()->university_id)->first();
        $userAvatarImageUrl = UploadImageFacade::getAssetImageUrl($request,Auth::id());
        return view('home',compact('userAvatarImageUrl','universityName'));
    }
}
