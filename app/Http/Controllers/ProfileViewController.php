<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile\Profile;
use Illuminate\Support\Facades\Auth;

class ProfileViewController extends Controller
{
    public function view($id)
    {
        $user = Profile::getUserProfileData($id);
        $userAvatarImageUrl = Profile::getUserProfileImageData(request(),Auth::id());

        $profileImage = Profile::getUserProfileImageData(request(),$id);
        return view('profile/view',compact('user','userAvatarImageUrl','profileImage'));
    }
}
