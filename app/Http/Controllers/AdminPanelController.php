<?php

namespace App\Http\Controllers;

use App\Admin\AdminMiddleware;
use App\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;

class AdminPanelController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin.verified');
    }

    public function index(Request $request)
    {
        if (AdminMiddleware::getAdminSessionCheckedData($request)) {
            $users = DB::table('users')
                ->join('universities', 'users.university_id', '=', 'universities.id')
                ->join('countries', 'users.country_id', '=', 'countries.id')
                ->select('users.*','universities.university_name','countries.country_name')
                ->paginate(15);
            return view('admin/admin_panel', compact('users'));
        }
        return AdminMiddleware::envAdminRedirect();
    }

    protected function adminLogout(Request $request)
    {
        AdminMiddleware::setAdminSessionCheckedData($request, false);
        return AdminMiddleware::envAdminRedirect();
    }
}
