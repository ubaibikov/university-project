<?php

namespace App\Http\Controllers;

use App\University;
use App\Universities\Universities;
use Illuminate\Http\Request;


class UniversityController extends Controller
{

    public static $currentUniversityPath = 'admin/admin_panel/';

    public function index()
    {
        $universities = University::all();
        return view(self::$currentUniversityPath . 'universities', compact('universities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $universityValidate = Universities::getUnivercityValiation($request);

        $universityMake = new University([
            'university_name' => $request->get('universityName'),
        ]);

        $universityMake->save();

        return Universities::universityMainRedirect()->with('success', 'Contact saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $university = Universities::unifersityFind($id);
        return view(self::$currentUniversityPath . 'university_update', compact('university'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $editdata = [
            'university_name' => $request->universityName,
        ];
        University::where('id', $id)->update($editdata);
        return Universities::universityMainRedirect()->with('success', 'Contact saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $university = Universities::unifersityFind($id);
        $university->delete();

        return Universities::universityMainRedirect()->with('success', 'Contact saved!');
    }
}
