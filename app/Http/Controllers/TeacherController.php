<?php

namespace App\Http\Controllers;

use App\Country;
use App\Teacher;
use App\University;
use App\User;
use Illuminate\Http\Request;

class TeacherController extends Controller
{
    use \App\Admin\AdminTrait;

    public static $basePrefix = 'teacher';
    public static $baseRedirect = '/admin_panel/teachers';

    public function index()
    {
        $universities = University::all();
        $countries = Country::all();
        return view(self::$currentAdminPath . 'teachers', compact('universities', 'countries'));
    }

    public function create(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'patronymic' => ['nullable', 'string', 'max:255'],
            'email' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:2'],
            'university_id' => ['nullable', 'int'],
            'country_id' => ['nullable', 'int'],
        ]);

        Teacher::create([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'patronymic' => $request->patronymic,
            'email' => $request->email,
            'password' => $request->password,
            'university_id' => $request->university_id,
            'country_id' => $request->country_id,
        ]);

        return redirect(self::$baseRedirect);
    }
    public function edit($id)
    {
        Teacher::all()->where('id', $id)->first();
        return view(self::$currentAdminPath . 'teacher_update');
    }
    public function destroy(int $id)
    {
        $teacher = Teacher::find($id);
        $teacher->delete();

        return $this->adminPanelRedirect('/teachers');
    }
}
