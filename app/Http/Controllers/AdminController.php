<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Admin\AdminMiddleware;

class AdminController extends Controller
{

    protected static $ADMIN_PATH = 'admin/';



    public function index()
    {
        if(AdminMiddleware::getAdminSessionCheckedData(request())){
           return AdminMiddleware::adminPanelRedirect();
        }
        return view(self::$ADMIN_PATH . 'admin_login');
    }

    public function show(Request $request)
    {
        $request->validate([
            'adminEmail' => 'required|max:255',
            'adminPassword' => 'required|min:2',
        ]);

        return view(self::$ADMIN_PATH . 'admin_login');
    }

    public function panel(Request $request)
    {
        return view(self::$ADMIN_PATH . 'admin_panel');
    }
}
