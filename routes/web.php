<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Admin\AdminPanel;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/admin', 'AdminController@index');
Route::post('/admin', 'AdminController@show')->name('admin')->middleware('admin.verified');

AdminPanel::routes();

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home/profile/{id}','ProfileEditController@profile')->name('profile-edit')->middleware('profile.check');
Route::post('/home/profile/{id}','ProfileEditController@update')->name('profile-update')->middleware('profile.check');
Route::get('/profile/{id}','ProfileViewController@view')->name('profile-view');
