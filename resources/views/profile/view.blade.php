@extends('layouts.app')

@section('content')
<div>
    <div class="container profile-short">
        <div class="d-flex justify-content-center profile">
            <div class="user-profile-avatar" style="background-image:url('{{$profileImage}}')"></div>
            <form class="text-center">
                <div id="profile">
                    <h3>{{$user->name}}</h3>
                    <h3>{{$user->email}}</h3>
                    <h3>{{$user->university_name}}</h3>
                    <h3>{{$user->country_name}}</h3>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<!-- Replace to vue js-->
<style>
    .user-profile-avatar{
            width: 200px;
            height: 200px;
            background-size: cover;
            background-position: top center;
            border-radius: 50%;
            margin-right: 10px;
    }
    .profile-edit__input , .profile-edit__input:focus{
        border: none;
        outline: none;
        text-align: center;
        padding: 5px;
    }
    .profile-edit__margin{
        margin: 5px;
    }
</style>
