@extends('layouts.app')

@section('content')
<div>
    <div class="container profile-short">
        <div class="d-flex justify-content-center profile">
                @csrf
                <div id="profile">
                <form class="text-center" action="{{route('profile-update',$user->id)}}" method="POST">
                    <div class="user-profile-avatar" style="background-image:url('{{$userAvatarImageUrl}}')"></div>

                    <div class="form-row">
                        <div class="col">
                          <input type="text" class="form-control" placeholder="First name">
                        </div>
                        <div class="col">
                          <input type="text" class="form-control" placeholder="Last name">
                        </div>
                      </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

<!-- Replace to vue js-->
<style>
    .user-profile-avatar{
            width: 200px;
            height: 200px;
            background-size: cover;
            background-position: top center;
            border-radius: 50%;
            margin-right: 10px;
    }
    .profile-edit__input , .profile-edit__input:focus{
        border-size: 3px;
        outline: none;
        text-align: center;
    }
    .profile-edit__margin{
        margin: 5px;
    }
</style>
