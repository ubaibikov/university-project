@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Admin Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('admin') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="adminEmail" class="col-md-4 col-form-label text-md-right">{{ __('Admin Login') }}</label>

                            <div class="col-md-6">
                                <input id="adminEmail" type="email" class="form-control @error('adminEmail') is-invalid @enderror" name="adminEmail" value="{{ old('adminEmail') }}" required autocomplete="adminEmail" autofocus>

                                @error('adminEmail')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="adminPassword" class="col-md-4 col-form-label text-md-right">{{ __('Admin Password') }}</label>

                            <div class="col-md-6">
                                <input id="adminPassword" type="password" class="form-control @error('adminPassword') is-invalid @enderror" name="adminPassword" required autocomplete="adminPassword">

                                @error('adminPassword')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

