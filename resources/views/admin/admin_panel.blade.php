@extends('layouts.admin')
@section('adminNavBar')
<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#">Universities</a>
                </li>

                <li class="nav-item">
                <a class="nav-link" href="#">Countries</a>
                </li>

                <li class="nav-item">
                <a class="nav-link" href="{{route('adminPanelUsers')}}">Users</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="#">Teachers</a>
                </li>

                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                       admin <span class="caret"></span>
                    </a>

                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ route('adminLogout') }}"
                           onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('adminLogout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">User Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">University</th>
                    <th scope="col">Country</th>
                  </tr>
                </thead>
                <tbody>
                @foreach ($users as $rowId => $user)
                  <tr>
                    <th scope="row">{{$user->id}}</th>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td><a href="{{route('universities.edit',$user->university_id)}}"> {{$user->university_name}} </a></td>
                    <td><a href="{{route('countries.edit',$user->country_id)}}"> {{$user->country_name}} </a></td>
                  </tr>
                  @endforeach
                  {{ $users->links() }}
                </tbody>
              </table>
        </div>
    </div>
</div>

@endsection
