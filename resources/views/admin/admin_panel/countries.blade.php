@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Panel Settings</div>

                <div class="card-body">
                    <form action="{{route('countries.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">country name</label>
                                <input type="text" class="form-control" id="countryName" name="countryName" aria-describedby="emailHelp">
                            </div>
                            <button  class="btn btn-primary" type="submit">Send</button>
                        </form>
                </div>
                @foreach ($countries as $country)
                    <ul>
                    <li> <a href="{{ route('countries.edit',$country->id) }}">{{$country->country_name}}</a>
                        <form action="{{ route('countries.destroy', $country->id)}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger" type="submit">Delete</button>
                      </form></li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
