@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Panel Settings</div>

                <div class="card-body">
                    <form action="{{route('universities.update',$university->id)}}" method="POST">
                            @method('PATCH')
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">University name</label>
                                <input type="text" class="form-control" id="universityName" name="universityName" aria-describedby="emailHelp" value="{{$university->university_name}}">
                            </div>
                            <button  class="btn btn-primary" type="submit">Send</button>
                        </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
