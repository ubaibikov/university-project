@extends('layouts.admin')

@section('title')
    Users
@endsection

@section('content')

<div class="container">
    <table class="table table-dark">
        <thead>
          <tr>
            <th scope="row">#</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">University</th>
            <th scope="col">Country</th>
            <th scope="col">Handle</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->university_name}}</td>
                <td>{{$user->country_name}}</td>
                <td>
                <a href="{{route('profile',$user->id)}}" class="btn btn-info">View</a>
                <button class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('delete-form-{{$user->id}}').submit();">Delete</button>
                    <form action="{{route('adminPanelUsersDelete',$user->id)}}" class="d-none" id="delete-form-{{$user->id}}" method="POST">
                        @method('DELETE')
                        @csrf
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
</div>

@endsection
