@extends('layouts.admin')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Admin Panel Settings</div>

                <div class="card-body">
                    <form action="{{route('universities.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="exampleInputEmail1">University name</label>
                                <input type="text" class="form-control" id="universityName" name="universityName" aria-describedby="emailHelp">
                            </div>
                            <button  class="btn btn-primary" type="submit">Send</button>
                        </form>
                </div>
                @foreach ($universities as $university)
                    <ul>
                    <li> <a href="{{ route('universities.edit',$university->id) }}">{{$university->university_name}}</a>
                        <form action="{{ route('universities.destroy', $university->id)}}" method="post">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-danger" type="submit">Delete</button>
                      </form></li>
                    </ul>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection
