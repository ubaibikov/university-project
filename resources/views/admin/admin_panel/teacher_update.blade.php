@extends('layouts.admin')

@section('title')
    Teacher:: Update
@endsection

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create TeacherName</div>
                <div class="card-body">
                    <form action="{{ route('teachers.create') }}">
                        @method('POST')
                        @csrf
                        <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="teacherName">Name</label>
                            <input type="text" class="form-control" id="teacherName" name="name" placeholder="Teacher Name" value="{{$teacher->name}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="lastName">Last Name</label>
                            <input type="text" class="form-control" id="lastName" name="last_name" placeholder="Teacher Password" value="{{$teacher->password}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="inputEmail4">Patronymic</label>
                            <input type="text" class="form-control" id="teacherPatronymic" name="patronymic" placeholder="Teacher Patronymic" value="{{$teacher->patronymic}}">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="teacherEmail">Email</label>
                            <input type="email" class="form-control" id="teacherEmail" name="email" placeholder="Teacher Email" value="{{$teacher->email}}">
                        </div>

                        <div class="form-group col-md-6">
                            <label for="teacherPassword">Password</label>
                            <input type="password" class="form-control" id="teacherPassword" name="password" placeholder="Password" value="{{$teacher->password}}">
                        </div>
                    </div>
                    <div class="form-row">

                        <div class="form-group col-md-6">
                            <label for="inputState">Country</label>
                            <select id="inputState" name="country_id" class="form-control">
                            @foreach ($countries as $country)
                                <option value="{{$country->id}}" @if ($country->id == $teacher->country_id) selected @endif>{{$country->country_name}}</option>
                            @endforeach
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label for="inputState">University</label>
                            <select id="inputState" name="university_id" class="form-control">
                              @foreach ($universities as $university)
                                    <option value="{{$university->id}}" @if ($country->id == $teacher->university_id) selected @endif >{{$university->university_name}}</option>
                              @endforeach
                            </select>
                        </div>

                    </div>
                        <button type="submit" class="btn btn-primary">Create</button>
                      </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
